One more member of the HAAP (HAcker APpetite) Project

A command bassed game in which you explore cyberspace node to node. Downloading valuable intel from databases and shutdown systems for the sake of being a famous hacker.

You cand find information about how to play in https://bitbucket.org/juliancanaves/haap/wiki/browse

This particular version of the game will be written in golang and it will be a terminal software first.

As I've tried to implement my original idea in different stacks, I wish GO gives me the flexibility I didn't find in other languages.

---

Based on vintage cyberpunk culture.

ICE stands for Intrusion Countermeasures Electronics,
term popularized by William Gibson, originally coined by Tom Maddox
[https://en.wikipedia.org/wiki/Intrusion_Countermeasures_Electronics]


With grattitude.

---

Libraries used:
- tview for the layout (https://github.com/rivo/tview)
- termloop for map command (https://github.com/JoelOtter/termloop)

