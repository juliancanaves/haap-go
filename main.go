package main

var Glyphs = []rune{
	'ア', 'イ', 'ウ', 'エ', 'オ',
	'カ', 'キ', 'ク', 'ケ', 'コ',
	'サ', 'シ', 'ス', 'セ', 'ソ',
	'タ', 'チ', 'ツ', 'テ', 'ト',
	'ナ', 'ニ', 'ヌ', 'ネ', 'ノ',
	'ハ', 'ヒ', 'フ', 'ヘ', 'ホ',
	'マ', 'ミ', 'ム', 'メ', 'モ',
	'ラ', 'リ', 'ル', 'レ', 'ロ',
	'ヤ', 'ユ', 'ヨ',
	'ワ', 'ヲ', 'ン',
	'0', '1', '2', '3', '4',
	'5', '6', '7', '8', '9',
}

var spriteFallsByDrop = make(map[int]*Heap)

func main() {

	go func() {

		println("new dropfall")

		for i := 3; i < 5; i++ {
			hp := &Heap{
				column:  i,
				stop:    make(chan bool, 1),
				sprites: make(map[*DropFall]bool),
				newDrop: make(chan bool, 1),
			}
			spriteFallsByDrop[newDrop] = hp

			go hp.drop()

			hp.newDrop <- true
		}

		hp := spriteFallsByDrop[4]

		delete(spriteFallsByDrop, 4)

		hp.stop <- true

	}()

}
