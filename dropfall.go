package main

import "math/rand"

var Glyphs = []rune{
	'ア', 'イ', 'ウ', 'エ', 'オ',
	'カ', 'キ', 'ク', 'ケ', 'コ',
	'サ', 'シ', 'ス', 'セ', 'ソ',
	'タ', 'チ', 'ツ', 'テ', 'ト',
	'ナ', 'ニ', 'ヌ', 'ネ', 'ノ',
	'ハ', 'ヒ', 'フ', 'ヘ', 'ホ',
	'マ', 'ミ', 'ム', 'メ', 'モ',
	'ラ', 'リ', 'ル', 'レ', 'ロ',
	'ヤ', 'ユ', 'ヨ',
	'ワ', 'ヲ', 'ン',
	'0', '1', '2', '3', '4',
	'5', '6', '7', '8', '9',
}

var spriteFallsByDrop = make(map[int]*Heap)

type DropFall struct {
	line    *Heap
	done    bool
	head    int
	pulsate bool
}

func (df *DropFall) drop() {
	var lastGlyph rune

DROP:
	for {
//		select {
		//		case <-df.pulsate:

//		default:

			//			if !df.done && df.head <= 3 {
			//case <-df.start:
			newGlyph := Glyphs[rand.Intn(len(Glyphs))]

			println(lastGlyph)
			println(newGlyph)

			lastGlyph = newGlyph

			df.head++
			//			} else {
			df.done = true
			//			}

			df.line.newDrop <- true

			break DROP
//		}
	}

	delete(df.line.sprites, df)
}

type Heap struct {
	column  int
	stop    chan bool
	sprites map[*DropFall]bool
	newDrop chan bool
}

func (hp *Heap) drop() {
	for {
		select {
		case <-hp.stop:
			println('o')
			return
		case <-hp.newDrop:
			df := &DropFall{
				line: hp,
			}

			hp.sprites[df] = true

			go df.drop()

		}
	}
}

func main() {

	go func() {

		println("new dropfall")

		for i := 0; i < 5; i++ {
			hp := &Heap{
				column:  i,
				stop:    make(chan bool, 1),
				sprites: make(map[*DropFall]bool),
				newDrop: make(chan bool, 1),
			}
			spriteFallsByDrop[i] = hp

			go hp.drop()

			hp.newDrop <- true
		}

		hp := spriteFallsByDrop[4]

		delete(spriteFallsByDrop, 4)

		hp.stop <- true

	}()

}
